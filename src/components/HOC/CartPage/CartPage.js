import React from 'react'
import { Helmet } from 'react-helmet'
import { Wrap } from './style'

export default (props) => {
  return (
    <Wrap>
      <Helmet>
        <html lang={`${props.languageCode}-UA`} />
        <title>Cart | Benjamin</title>
      </Helmet>
      <h1>Cart</h1>
      <p>Cart page</p>
    </Wrap>
  )
}
