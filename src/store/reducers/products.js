import * as at from 'store/actionTypes';

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case at.SET_PRODUCTS_INITIAL_STATE: {
      return initialState
    }
    case at.GET_PRODUCTS_SEARCH_SUCCESS: {
      let { languageCode, payload } = action

      return {
        ...state,
        total: payload.total,
        list: {
          [languageCode]: payload.list
        }
      }
    } default: {
      return state
    }
  }
}
