import * as at from 'store/actionTypes'

export const toggleMenuVisibility = () => (dispatch) => {
  dispatch({ type: at.APP_TOGGLE_MENU_VISIBILITY })
}

export const setProp = (key, value) => (dispatch, getStore) => {
  try {
    const store = getStore()

    if (value !== store.app[key]) {
      dispatch({ type: at.APP_SET_PROP, key, value })
    }
  } catch (e) {
    console.error(e)
  }
}

export const onLocationPathChange = (props) => (dispatch, getStore) => {
  try {
    const store = getStore()
    const { locationPath } = props

    if (locationPath !== store.app.locationPath) {
      const payload = { locationPath }
      const {
        locationPathWithoutLanguage,
        languageId,
        languageCode
      } = props

      if (locationPathWithoutLanguage !== store.app.locationPathWithoutLanguage) {
        payload.locationPathWithoutLanguage = locationPathWithoutLanguage
      }

      if (store.app.languageId !== languageId && store.app.languageCode !== languageCode) {
        payload.languageId = languageId
        payload.languageCode = languageCode
      }

      if (store.app.menuIsVisible) {
        dispatch(toggleMenuVisibility())
      }

      dispatch({ type: at.ON_LOCATION_PATH_CHANGE, payload })
    }
  } catch (e) {
    console.error(e)
  }
}
