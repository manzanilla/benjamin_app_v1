import React from 'react'
import { Helmet } from 'react-helmet'
import { Wrap } from './style'

export default (props) => {
  return (
    <Wrap>
      <Helmet>
        <html lang={`${props.languageCode}-UA`} />
        <title>Home | Benjamin</title>
      </Helmet>
      <h1>Home</h1>
      <p>Home page</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam architecto atque consequatur doloremque earum eligendi eum illo in, laudantium magnam molestiae necessitatibus nemo nisi odit possimus, quos reiciendis sed totam!</p>
    </Wrap>
  )
}
