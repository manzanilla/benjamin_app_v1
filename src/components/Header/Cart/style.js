import styled from 'styled-components'
import { CartFill as StyledCart } from '@styled-icons/bootstrap/CartFill'
import { Link } from 'wouter'

export const CartIcon = styled(StyledCart)`
  color: #87ceeb;
  width: 24px;
  height: 24px;
`

export const Wrap = styled(Link)`
  display: block;
`
