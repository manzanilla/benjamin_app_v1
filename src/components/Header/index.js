import { connect } from 'react-redux'
import Header from './Header'

const mapStateToProps = (state) => ({
  menuIsVisible: state.app.menuIsVisible,
  languageCode: state.app.languageCode,
  categoriesLanguageCode: state.categories.languageCode,
  categoriesTree: state.categories.tree
})

export default connect(mapStateToProps)(Header)
