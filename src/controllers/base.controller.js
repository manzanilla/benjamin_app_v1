import createStore from 'store/createStore'
import * as appActionCreators from 'store/actions/app'
import * as categoriesActionCreators from 'store/actions/categories'
import {
  getLocationPathWithoutLanguage,
  getLanguageInfoFromLocationPath
} from 'helpers/language'

export default (cb) => async (ctx) => {
  try {
    const store = createStore()
    const { dispatch } = store

    const {
      APP_HOST,
      STATIC_HOST,
      STATIC_HOST_LOCAL,
      API_V1_HOST,
      NODE_ENV
    } = ctx.app.env

    const { id: languageId, code: languageCode } = getLanguageInfoFromLocationPath(ctx.path)

    dispatch(appActionCreators.setProp('mode', NODE_ENV))
    dispatch(appActionCreators.setProp('locationPath', ctx.path))
    dispatch(appActionCreators.setProp('locationPathWithoutLanguage', getLocationPathWithoutLanguage(languageId, ctx.path)))
    dispatch(appActionCreators.setProp('languageId', languageId))
    dispatch(appActionCreators.setProp('languageCode', languageCode))
    dispatch(categoriesActionCreators.setLanguageCode(languageCode))
    dispatch(appActionCreators.setProp('hostApp', APP_HOST))
    dispatch(appActionCreators.setProp('hostStatic', STATIC_HOST))
    dispatch(appActionCreators.setProp('hostStaticLocal', STATIC_HOST_LOCAL))
    dispatch(appActionCreators.setProp('hostApiV1', API_V1_HOST))

    await dispatch(categoriesActionCreators.getCategoriesTree(languageCode))

    dispatch(categoriesActionCreators.setTreeMap(languageCode))

    const props = {
      ctx,
      store
    }

    return cb(props)
  } catch (e) {
    console.error(e)
  }
}
