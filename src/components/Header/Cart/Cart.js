import React from 'react'
import { Wrap, CartIcon } from './style'
import { getLinkHrefByLanguageCode } from 'helpers/language'

export default (props) => {
  return (
    <Wrap
      to={getLinkHrefByLanguageCode(props.languageCode, 'cart')}
      className="cart"
    >
      <a>
        <CartIcon  />
      </a>
    </Wrap>
  )
}
