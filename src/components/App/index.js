import App from './App'
import { connect } from 'react-redux'
import { onLocationPathChange } from 'store/actions/app'

const mapStateToProps = (state) => ({
  locationPath: state.app.locationPath,
  languageId: state.app.languageId
})

const mapDispatchToProps = (dispatch) => ({
  onLocationPathChange: (props) => dispatch(onLocationPathChange(props))
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
