import React, { useState, useEffect } from 'react'
import { Helmet } from 'react-helmet'
import { ContentWrap } from './style'
import Breadcrumbs from 'components/Breadcrumbs'
import SubCategoriesMenu from './SubCategoriesMenu'
import * as categoriesHelper from 'helpers/categories'
import { Container } from 'helpers/style'
import ProductList from './ProductList'

export default (props) => {
  const {
    categoryId,
    categories: {
      languageCode,
      treeMap: {
        [categoryId]: treeMapItem = []
      } = {},
      tree: {
        [languageCode]: tree = []
      } = {}
    } = {}
  } = props

  const [ stateCategoryId, setStateCategoryId ] = useState(categoryId)
  const [ stateLanguageCode, setStateLanguageCode ] = useState(languageCode)

  useEffect(() => {
    setBreadcrumbs(props)

    return () => {
      unsetBreadcrumbs(props)
    }
  }, [])

  useEffect(() => {
    if (languageCode !== stateLanguageCode) {
      setStateLanguageCode(languageCode)
      setBreadcrumbs(props)
    }
  }, [ languageCode ])

  useEffect(() => {
    if (categoryId !== stateCategoryId) {
      setStateCategoryId(categoryId)
      setBreadcrumbs(props)
    }
  }, [ categoryId ]) // at on change identifier of category

  let h1 = '', el

  for (const index in treeMapItem) {
    const elIndex = treeMapItem[index]
    el = el ? el.children[elIndex] : tree[elIndex]
    h1 = el.h1 ? el.h1 : el.name
  }

  return (
    <>
      <Helmet>
        <html lang={`${props.languageCode}-UA`} />
        <title>{h1} | Benjamin</title>
        <meta name="description" content={`${h1} | Benjamin`} />
      </Helmet>
      <div style={{
        background: 'rgb(135 206 235 / 25%)',
        padding: '32px 0',
        marginTop: '-32px',
        boxShadow: '0px 0px 8px 0px rgba(34, 60, 80, 0.4)'
      }}>
        <Container>
          <h1>{h1}</h1>
        </Container>
        <Breadcrumbs />
        <SubCategoriesMenu categoryId={categoryId} />
      </div>
      <ContentWrap>
        <Container>
          <ProductList
            categoryId={categoryId}
            languageCode={props.languageCode}
          />
        </Container>
      </ContentWrap>
    </>
  )
}

const setBreadcrumbs = (props) => {
  const {
    categoryId,
    categories: {
      languageCode,
      treeMap,
      tree: {
        [languageCode]: tree = []
      } = {}
    } = {}
  } = props

  const breadcrumbs = categoriesHelper.getBreadcrumbs({
    languageCode,
    categoryId,
    tree,
    treeMap
  })

  props.setBreadcrumbs(breadcrumbs)
}

const unsetBreadcrumbs = (props) => {
  props.setBreadcrumbs({})
}
