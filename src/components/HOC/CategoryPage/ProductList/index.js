import { connect } from 'react-redux'
import ProductList from './ProductList'
import * as productsActionCreators from 'store/actions/products'

const mapStateToProps = (state) => {
  const { list = [] } = state.products
  const { hostStatic } = state.app

  return { list, hostStatic }
}

const mapDispatchToProps = (dispatch) => ({
  search: (languageCode, categoryId) => {
    const { getProductsSearch } = productsActionCreators
    return dispatch(getProductsSearch(languageCode, categoryId))
  },
  setInitialState: () => dispatch(productsActionCreators.setInitialState())
})

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)
