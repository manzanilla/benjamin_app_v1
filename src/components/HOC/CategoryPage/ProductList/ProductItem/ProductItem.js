import React, { useState, useEffect } from 'react'
import {
  Wrap,
  Placeholder,
  Title,
  Button,
  Price,
  Img
} from './style'
import { Link } from 'wouter'
import { getLinkHrefByLanguageCode } from 'helpers/language'

export default (props) => {
  const { id: productId } = props
  const href = `p${productId}`
  const to = getLinkHrefByLanguageCode(props.languageCode, href)

  const [ imageOpacity, setImageOpacity ] = useState(0)
  const [ imageIsLoaded, setImageIsLoaded ] = useState(false)

  useEffect(() => {
    if (props.imageURL) {
      const img = new Image()
      img.src= props.imageURL
      img.onload = function() {
        setImageIsLoaded(true)
      }
    }
  }, [])

  useEffect(() => {
    if (imageIsLoaded === true) {
      setTimeout(() => setImageOpacity(1), 300)

    }
  }, [ imageIsLoaded ])

  return (
    <Wrap>
      {imageIsLoaded ? (
        <MainImg
          to={to}
          src={props.imageURL}
          opacity={imageOpacity}
        />
      ): (
        <Placeholder to={to} />
      )}
      <Title to={to}>{props.h1}</Title>
      <Price>
        <span>{props.price}</span> грн
      </Price>
      <Button className="cart-add" data-id={productId}>
        Купить
      </Button>
    </Wrap>
  )
}

function MainImg(props) {
  return (
    <Link to={props.to}>
      <a className="main-img">
        <Img
          src={props.src}
          alt=""
          style={{ opacity: props.opacity }}
        />
      </a>
    </Link>
  )
}
