import React from 'react'
React.useLayoutEffect = React.useEffect
import { Container, Wrap } from './style'
import { ScrollMenu } from 'react-horizontal-scrolling-menu'
import { Link } from 'wouter'
import { useDrag } from 'helpers/react-horizontal-scrolling-menu'
import { getLinkHrefByLanguageCode } from 'helpers/language'

export default (props) => {
  const { dragStart, dragStop } = useDrag()
  const {
    categoriesLanguageCode,
    categoryId,
    tree: {
      [categoriesLanguageCode]: tree = []
    } = {},
    treeMap: {
      [categoryId]: treeMap = []
    } = {}
  } = props

  let treeTemp = tree
  let treeEl = {}
  for (let ix = 0; ix < treeMap.length; ix++) {
    const treeIx = treeMap[ix]
    treeEl = treeTemp[treeIx]

    treeTemp = treeEl.children
  }

  if (!treeEl.children) return null

  const { children: items = [] } = treeEl

  return (
    <Container>
      <div onMouseLeave={dragStop}>
        <ScrollMenu
          onMouseDown={() => dragStart}
          onMouseUp={() => dragStop}
        >
          <Wrap>
            {items.map((el) => {
              const { id, name } = el
              const href = `c${id}`

              return (
                <Link
                  key={id}
                  to={getLinkHrefByLanguageCode(categoriesLanguageCode, href)}
                >
                  {name}
                </Link>
              )
            })}
          </Wrap>
        </ScrollMenu>
      </div>
    </Container>
  )
}
