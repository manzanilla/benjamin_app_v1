import * as at from 'store/actionTypes';

const initialState = {
  menuIsVisible: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case at.APP_SET_PROP: {
      let { key, value } = action

      return {
        ...state,
        [key]: value
      }
    } case at.APP_TOGGLE_MENU_VISIBILITY: {
      return {
        ...state,
        menuIsVisible: !state.menuIsVisible
      }
    } case at.ON_LOCATION_PATH_CHANGE: {
      let { payload } = action

      return {
        ...state,
        ...payload
      }
    } default: {
      return state;
    }
  }
}
