import React from 'react'
import { Helmet } from 'react-helmet'
import { Wrap } from './style'

export default (props) => {
  return (
    <Wrap>
      <Helmet>
        <html lang={`${props.languageCode}-UA`} />
        <title>About | Benjamin</title>
      </Helmet>
      <h1>About</h1>
      <p>About page</p>
    </Wrap>
  )
}
