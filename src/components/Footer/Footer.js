import React from 'react'
import { Wrap } from './style'
import { Container } from 'helpers/style'

export default () => {
  return (
    <Wrap>
      <Container>
        Footer
      </Container>
    </Wrap>
  )
}
