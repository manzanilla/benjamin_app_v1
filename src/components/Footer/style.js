import styled from 'styled-components'

export const Wrap = styled.div`
  flex-shrink: 0;
  padding: 16px 0;
  background: rgb(135 206 235 / 25%);
  box-shadow: 0 0 8px 0 rgba(34, 60, 80, 0.4);
`
