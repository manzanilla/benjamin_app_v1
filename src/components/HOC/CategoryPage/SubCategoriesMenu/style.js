import styled from 'styled-components'
import { Container as ContainerHelper } from 'helpers/style'

export const Container = styled(ContainerHelper)`
  overflow: hidden;
`

export const Wrap = styled.div`
  margin-top: 12px;
  display: flex;
  flex-direction: row;
  a {
    margin-right: 8px;
    color: #000;
    background: #fff;
    border: 2px solid #87ceeb;
    box-sizing: border-box;
    display: flex;
    height: 40px;
    border-radius: 4px;
    padding: 0 16px;
    align-items: center;
    font-size: 12px;
    text-transform: uppercase;
    font-weight: bold;
    :last-child {
      margin-right: 0;
    }
  }
`
