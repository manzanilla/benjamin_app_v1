import { connect } from 'react-redux'
import ProductPage from './ProductPage'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode
})

export default connect(mapStateToProps)(ProductPage)
