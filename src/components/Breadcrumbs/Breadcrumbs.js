import React from 'react'
React.useLayoutEffect = React.useEffect
import { Container, Wrap, Separator } from './style'
import { ScrollMenu } from 'react-horizontal-scrolling-menu'
import { Home } from '@styled-icons/boxicons-solid/Home'
import { Link } from 'wouter'
import { useDrag } from 'helpers/react-horizontal-scrolling-menu'

export default (props) => {
  const { dragStart, dragStop } = useDrag()
  const { breadcrumbs = {} } = props

  return (
    <Container>
      <div onMouseLeave={dragStop}>
        <ScrollMenu
          onMouseDown={() => dragStart}
          onMouseUp={() => dragStop}
        >
          <Wrap>
            {Object.values(breadcrumbs).map(({ url, name }, ix) => {
              if (!ix) {
                return (
                  <Link key={url} to={url}>
                    <a>
                      <Home fill="#fff" size={16} />
                    </a>
                  </Link>
                )
              }

              return (
                <React.Fragment key={url}>
                  <Separator size={8} />
                  <Link to={url}>
                    <a>
                      <span>{name}</span>
                    </a>
                  </Link>
                </React.Fragment>
              )
            })}
          </Wrap>
        </ScrollMenu>
      </div>
    </Container>
  )
}
