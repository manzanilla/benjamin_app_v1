import { connect } from 'react-redux'
import AboutPage from './AboutPage'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode
})

export default connect(mapStateToProps)(AboutPage)
