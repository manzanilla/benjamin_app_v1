import styled from 'styled-components'

export const Wrap = styled.div`
  padding: 64px 0 0;
  flex: 1 0 auto;
`
