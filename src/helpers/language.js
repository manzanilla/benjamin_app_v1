/**
 * Что бы поменять дефолтный или добавить новый язык
 * необходимо добавить (например) LANGUAGE_EN, добавить
 * LANGUAGE_EN в LANGUAGE_LIST и LANGUAGE_ID, изменить server.js,
 * изменить components/App.js
 * */

export const DEFAULT_LANGUAGE_ID = 2 // ID языка по умолчанию - `ru`
export const LANGUAGE_UK = { ID: 1, CODE: 'uk', LABEL: 'Укр' }
export const LANGUAGE_RU = { ID: 2, CODE: 'ru', LABEL: 'Рус', IS_DEFAULT: true }
// export const LANGUAGE_EN = { ID: 3, CODE: 'en' }

export const LANGUAGE_LIST = [
  LANGUAGE_RU,
  LANGUAGE_UK
]

export const LANGUAGE_ID = {}
export const LANGUAGE_CODE = {}

LANGUAGE_ID[LANGUAGE_UK.ID] = LANGUAGE_UK
LANGUAGE_ID[LANGUAGE_RU.ID] = LANGUAGE_RU

LANGUAGE_CODE[LANGUAGE_UK.CODE] = LANGUAGE_UK
LANGUAGE_CODE[LANGUAGE_RU.CODE] = LANGUAGE_RU

export const getLinkHrefByLanguageCode = (languageCode, href = '') => {
  const { ID: languageId } = LANGUAGE_CODE[languageCode]
  return getLinkHrefByLanguageId(languageId, href)
}

export const getLinkHrefByLanguageId = (languageId, href = '') => {
  if (isDefaultLanguage(languageId)) return '/'+href

  const { CODE } = LANGUAGE_ID[languageId]

  if (!href) return '/'+CODE
  return `/${CODE}/${href}`
}

export const isDefaultLanguage = (languageId) => {
  return languageId === DEFAULT_LANGUAGE_ID
}

export const getLocationPathWithoutLanguage = (languageId, locationPath) => {
  try {
    if (languageId === DEFAULT_LANGUAGE_ID) return locationPath
    return locationPath.substr(0,1)+locationPath.substr(4)
  } catch (e) {
    console.error(e)
  }
}

export const getLanguageInfoFromLocationPath = (path) => {
  try {
    path = path.substring(0, 4)

    for (const language of LANGUAGE_LIST) {
      const { ID, CODE } = language

      if (path.length === 4 && path === `/${CODE}/`)
        return { id: ID, code: CODE }
      if (path.length === 3 && path === `/${CODE}`)
        return { id: ID, code: CODE }
    }

    const { ID, CODE } = LANGUAGE_ID[DEFAULT_LANGUAGE_ID]

    return { id: ID, code: CODE }
  } catch (e) {
    console.error(e)
  }
}

/*export const getLanguageIdFromLocationPath = (path) => {
  try {
    path = path.substring(0, 4)

    for (const language of LANGUAGE_LIST) {
      const { ID, CODE } = language

      if (path.length === 4 && path === `/${CODE}/`)
        return ID
      if (path.length === 3 && path === `/${CODE}`)
        return ID
    }

    return DEFAULT_LANGUAGE_ID
  } catch (e) {
    console.error(e)
  }
}*/
