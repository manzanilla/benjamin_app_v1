import { getLinkHrefByLanguageCode } from 'helpers/language'

export const getBreadcrumbs = ({ categoryId, tree, treeMap, languageCode }) => {
  const map = treeMap[categoryId]
  const breadcrumbs = []

  for (const ix in map) {
    const treeIx = map[ix]
    const treeEl = tree[treeIx]
    const { name, id } = treeEl
    const url = getLinkHrefByLanguageCode(languageCode,`c${id}`)

    breadcrumbs.push({ url, name })

    tree = treeEl.children
  }

  breadcrumbs.unshift({ url: getLinkHrefByLanguageCode(languageCode, '') })

  return breadcrumbs
}
