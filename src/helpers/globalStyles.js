import { createGlobalStyle } from 'styled-components'
import media from 'styled-media-query'

const GlobalStyle = createGlobalStyle`
  html, body, #root {
    height: 100%;
  }
  #root {
    display: flex;
    flex-direction: column;
  }
  html, body, h1 {
    margin: 0;
    padding: 0;
  }
  body {
    color: #000;
    background-color: #fff;
    font:  normal normal 400 16px/1.5 Open-Sans, Helvetica, Sans-Serif, serif;
  }
  a {
    color: blue;
    text-decoration: none;
    :hover {
      text-decoration: none;
    }
  }
  h1 {
    font-size: 32px;
  }
  .tel {
    font-style: italic;
    font-size: 12px;
    color: #000;
  }
  ${media.lessThan("medium")`
    h1 {
      font-size: 24px;
    }  
  `}
`

export default GlobalStyle
