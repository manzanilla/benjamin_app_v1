import React from 'react'
import { Link } from 'wouter'
import { Wrap } from './style'
import { getLinkHrefByLanguageId } from 'helpers/language'

export default (props) => {
  return (
    <Wrap>
      <Link href={getLinkHrefByLanguageId(props.languageId)}>
        <span>B</span>
        <span>e</span>
        <span>n</span>
        <span>j</span>
        <span>a</span>
        <span>m</span>
        <span>i</span>
        <span>n</span>
      </Link>
    </Wrap>
  )
}
