import { connect } from 'react-redux'
import Logo from './Logo'

const mapStateToProps = (state) => ({
  languageId: state.app.languageId
})

export default connect(mapStateToProps)(Logo)
