import styled from 'styled-components'
import { Link as WouterLink } from 'wouter'
import React from 'react'
import media from 'styled-media-query'

export const Link = styled(WouterLink)`
  font-size: 14px;
  color: #000;
  &.active {
    border-top: 4px solid #87ceeb;
    margin-top: -8px;
    padding-top: 4px;
  }
`

export const HamburgerWrap = styled.button`
  height: inherit;
  margin: 0 0 0 8px;
  padding: 0 4px 0 28px;
  display: none;
  border-radius: 4px;
  position: relative;
  border: none;
  cursor: pointer;
  background: #87ceeb;
  align-items: center;
  font-size: 12px;
  &:before, &:after {
    position: absolute;
    content: '';
    width: 20px;
    left: 4px;
    height: 1px;
    background: #000;
  }
  &:before {
    top: 12px;
  }
  &:after {
    bottom: 12px;
  }
  &.active {
    :before {
      top: 50%;
      transform: rotate(45deg);
    }
    :after {
      top: 50%;
      bottom: unset;
      transform: rotate(-45deg);
    }  
  }
  ${media.lessThan("medium")`
      display: flex;
  `}
`

export const Wrap = styled.nav`
  margin: 16px 0;
  height: 32px;
  > div {
    height: inherit;
    display: flex;
    align-items: center;
    justify-content: space-around;
  }
`
