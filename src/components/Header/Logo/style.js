import styled from 'styled-components'
import  media from 'styled-media-query'

export const Wrap = styled.div`
  height: inherit;
  a {
    height: inherit;
    display: flex;
    width: 120px;
    background: skyblue;
    position: relative;
    justify-content: space-between;
    align-items: center;
    padding: 0 8px;
    box-sizing: border-box;
    border-radius: 4px;
    color: #000;
    font-size: 10px;
    text-transform: uppercase;
    ${media.lessThan("medium")`
      width: 30px;
      justify-content: center;
      span {
        display: none;
        &:first-child {
          display: inline;
        }
      }
    `}
  }
`
