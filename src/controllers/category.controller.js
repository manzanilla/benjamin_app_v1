import React from 'react'
import * as categoriesHelper from 'helpers/categories'
import * as breadcrumbsActionCreators from 'store/actions/breadcrumbs'
import { getProductsSearch } from 'store/actions/products'
import ssr from 'helpers/ssr'

export default async (props) => {
  const { ctx, store } = props

  let { id: categoryId } = ctx.params
  categoryId = +categoryId

  const state = store.getState()

  const {
    categories: {
      languageCode,
      treeMap,
      tree: {
        [languageCode]: tree = []
      } = {}
    } = {}
  } = state

  const breadcrumbs = categoriesHelper.getBreadcrumbs({
    languageCode,
    categoryId,
    tree,
    treeMap
  })

  const { dispatch } = store
  dispatch(breadcrumbsActionCreators.setBreadcrumbs(breadcrumbs))
  await dispatch(getProductsSearch(languageCode, categoryId))

  ctx.body = ssr(ctx, store)
}
