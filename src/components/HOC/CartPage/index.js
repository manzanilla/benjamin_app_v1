import { connect } from 'react-redux'
import CartPage from './CartPage'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode
})

export default connect(mapStateToProps)(CartPage)
