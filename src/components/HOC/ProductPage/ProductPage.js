import React from 'react'
import { Helmet } from 'react-helmet'
import { Container } from 'helpers/style'

export default (props) => {
  const { id: productId } = props

  return (
    <>
      <Helmet>
        <html lang={`${props.languageCode}-UA`} />
        <title>Product title | Benjamin</title>
        <meta name="description" content={`Product meta description | Benjamin`} />
      </Helmet>
      <Container>
        <h1>Product H1 #{productId}</h1>
      </Container>
    </>
  )
}
