import styled from 'styled-components'

export const Wrap = styled.div`
  display: grid;
  grid-column-gap: 8px;
  grid-row-gap: 8px;
  @media (max-width: 639px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media (min-width: 640px) and (max-width: 959px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media (min-width: 960px) {
    grid-template-columns: repeat(4, 1fr);
  }
`
