import styled from 'styled-components'
import { Link } from 'wouter'

export const Wrap = styled.div`
  position: relative;
  padding-bottom: 56px;
  border-radius: 4px 4px 0 0;
  button {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
  }
  .main-img {
    display: block;
    padding-bottom: 150%;
    background: #ddd;
    margin-bottom: 8px;
    border-radius: 4px 4px 0 0;
    position: relative;
    overflow: hidden;
    transition: opacity .5s ease-out;
    &:before, &:after {
      content: "";
      width: 100%;
      height: 2px;
      top: 50%;
      margin-top: -1px;
      background: #eee;
      position: absolute;
    }
    &:before {
      transform: rotate(60deg);
    }
    &:after {
      transform: rotate(-60deg);
    }
  }
  :hover .main-img {
    opacity: .8;
  }
`

export const Placeholder = styled(Link)`
  display: block;
  padding-bottom: 150%;
  background: #ddd;
  margin-bottom: 8px;
  border-radius: 4px 4px 0 0;
  position: relative;
  &:before, &:after {
    content: "";
    width: 100%;
    height: 2px;
    top: 50%;
    margin-top: -1px;
    background: #eee;
    position: absolute;
  }
  &:before {
    transform: rotate(60deg);
  }
  &:after {
    transform: rotate(-60deg);
  }
`

export const Img = styled.img`
  display: block;
  height: 100%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1;
  transition: opacity .5s ease-out;
`

export const Title = styled(Link)`
  font-size: 13px;
  color: #000;
`

export const Price = styled.div`
  color: #000;
  font-size: 14px;
  text-align: center;
  span {
    font-size: 19px;
    color: orangered;
    font-style: italic;
  }
`

export const Button = styled.button`
  height: 48px;
  color: #fff;
  font-size: 18px;
  text-transform: uppercase;
  background: orangered;
  border-radius: 4px;
  outline: none;
  border: none;
  margin: 0;
  padding: 0;
  cursor: pointer;
`
