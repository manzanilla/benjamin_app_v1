import ssr from 'helpers/ssr'

export default async (props) => {
  const { ctx, store } = props

  ctx.body = ssr(ctx, store)
}
