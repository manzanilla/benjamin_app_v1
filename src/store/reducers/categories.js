import * as at from 'store/actionTypes';

const initialState = {
  tree: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case at.GET_CATEGORIES_TREE_SUCCESS: {
      let { languageCode, tree } = action

      return {
        ...state,
        tree: {
          ...state.tree,
          [languageCode]: tree
        }
      }
    } case at.CATEGORIES_SET_LANGUAGE_CODE: {
      let { languageCode } = action

      return {
        ...state,
        languageCode
      }
    } case at.SET_CATEGORIES_TREE_MAP: {
      let { treeMap } = action

      return {
        ...state,
        treeMap
      }
    } default: {
      return state
    }
  }
}
