import styled from 'styled-components'

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  width: 48px;
  justify-content: space-between;
  height: inherit;
  a {
    height: inherit;
    position: relative;
    display: flex;
    align-items: center;
    font-size: 10px;
    text-transform: uppercase;
    color: #656565;
    &.active {
      color: #000;
      cursor: default;
      &:before {
        content: '';
        position: absolute;
        left: 0;
        width: 100%;
        top: -8px;
        height: 4px;
        background: skyblue;
      }
    }
  }

  > * {
    margin-right: 8px;

    :last-child {
      margin-right: 0;
    }
  }
`
