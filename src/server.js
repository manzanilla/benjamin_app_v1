import Koa from 'koa'
// import serve from 'koa-static'
import Router from 'koa-router'
import dotenv from 'dotenv'
// import { resolve } from 'path'

import baseController from 'controllers/base.controller'
import indexController from 'controllers/index.controller'
import aboutController from 'controllers/about.controller'
import cartController from 'controllers/cart.controller'
import categoryController from 'controllers/category.controller'
import productController from 'controllers/product.controller'
import { LANGUAGE_UK } from 'helpers/language'
import { log } from 'helpers/console'
import { resolve } from 'path'

dotenv.config({ path: resolve(__dirname, '..', '.env') })

const { APP_IP, APP_PORT } = process.env

const app = new Koa()
const router = new Router({ sensitive: true })

// app.use(serve(resolve(__dirname, '..', 'dist', 'public')))
// app.use(serve(resolve(__dirname, '..', 'static')))

app.env = process.env

router.get('/', baseController(indexController))
router.get(`/${LANGUAGE_UK.CODE}`, baseController(indexController))
router.get('/about', baseController(aboutController))
router.get(`/${LANGUAGE_UK.CODE}/about`, baseController(aboutController))
router.get('/cart', baseController(cartController))
router.get(`/${LANGUAGE_UK.CODE}/cart`, baseController(cartController))
router.get('/c:id(\\d):rest(.*)', baseController(categoryController))
router.get(`/${LANGUAGE_UK.CODE}/c:id(\\d):rest(.*)`, baseController(categoryController))
router.get('/p:id(\\d)', baseController(productController))
router.get(`/${LANGUAGE_UK.CODE}/p:id`, baseController(productController))

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(APP_PORT, () => {
    const message = `Server listening on http://${APP_IP}:${APP_PORT}`

    log(message)
  })
