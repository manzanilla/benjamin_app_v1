import React, { useState, useEffect } from 'react'
import {
  Wrap,
  HamburgerWrap,
  Link
} from './style'
import { Container } from 'helpers/style'
import { getLinkHrefByLanguageCode } from 'helpers/language'
import { useRoute } from 'wouter'

export default (props) => {
  const [ languageCode, setLanguageCode ] = useState(props.languageCode)

  useEffect(() => {
    if (!props.isMain !== true) return

    if (props.languageCode !== languageCode) {
      if (!props.categoriesTree.hasOwnProperty(props.languageCode)) {
        props.getCategoriesTree(props.languageCode).then(() => {
          setLanguageCode(props.languageCode)
          props.categoriesSetLanguageCode(props.languageCode)
        })
      } else {
        setLanguageCode(props.languageCode)
        props.categoriesSetLanguageCode(props.languageCode)
      }
    }
  }, [ props.languageCode ])

  const {
    categoriesTree: {
      [props.categoriesLanguageCode]: categoriesTree = []
    } = {}
  } = props

  const linksJSX = categoriesTree.map((el) => {
    const href = `c${el.id}`
    return (
      <Link
        key={href}
        to={getLinkHrefByLanguageCode(props.categoriesLanguageCode, href)}
      >
        {el.name}
      </Link>
    )
  })

  if (props.context === 'header-horizontal') {
    return categoriesTree.map((el) => {
      const to = getLinkHrefByLanguageCode(props.categoriesLanguageCode, `c${el.id}`)
      const [ isActive ] = useRoute(to)
      const className = isActive
        ? 'category-link-medium active'
        : 'category-link-medium'

      return (
        <Link
          className={className}
          key={to}
          to={to}
        >
          {el.name}
        </Link>
      )
    })
  } else if (props.context === 'header-hamburger') {
    const attrs = {
      onClick: toggleMenuVisibility(props),
      className: 'header-hamburger'
    }

    if (props.menuIsVisible) {
      attrs.className += ' active'
    }

    return (
      <HamburgerWrap {...attrs}>
        МЕНЮ
      </HamburgerWrap>
    )
  }

  return (
    <Wrap>
      <Container>
        {linksJSX}
      </Container>
    </Wrap>
  )
}

const toggleMenuVisibility = (props) => () => {
  props.toggleMenuVisibility()
}
