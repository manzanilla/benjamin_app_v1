import { connect } from 'react-redux'
import Nav from './Nav'
import * as appActionCreators from 'store/actions/app'
import * as categoriesActionCreators from 'store/actions/categories'

const mapStateToProps = (state) => ({
  menuIsVisible: state.app.menuIsVisible,
  languageCode: state.app.languageCode,
  categoriesLanguageCode: state.categories.languageCode,
  categoriesTree: state.categories.tree
})

const mapDispatchToProps = (dispatch) => {
  return {
    toggleMenuVisibility: () => dispatch(appActionCreators.toggleMenuVisibility()),
    getCategoriesTree: (languageCode) => dispatch(categoriesActionCreators.getCategoriesTree(languageCode)),
    categoriesSetLanguageCode: (languageCode) => dispatch(categoriesActionCreators.setLanguageCode(languageCode))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Nav)
