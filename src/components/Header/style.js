import React from 'react'
import styled from 'styled-components'
import media from 'styled-media-query'

export const Wrap = styled.header`
  padding: 8px 0;
  background: #fff;
  box-shadow: 0 0 8px 0 rgba(34, 60, 80, 0.4);
  position: fixed;
  top: 0;
  z-index: 101;
  width: 100%;
  left: 0;
  .tel {
    height: inherit;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-right: 16px;
  }
  .tel span { display: inline; }
  .language-switcher, .cart {
    margin-left: 16px;
  }
  .first-container {
    height: 32px;
    display: flex;
    justify-content: space-between;
    > * {
      height: inherit;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  .last-container {
    display: none;
    &.active {
      padding: 16px 8px 8px 8px;
      flex-wrap: wrap;
      > * {
        flex: 50%;
        text-align: center;
        :nth-child(4n+1), :nth-child(4n+2) {
          padding-bottom: 8px;
        }
      }
    }
  }
  ${media.lessThan("medium")`
    a.category-link-medium { display: none; }
    .last-container.active { display: flex; }
    .tel span { display: none; }
  `}
`
