import styled from 'styled-components'
import { Container } from 'helpers/style'

export const ContentWrap = styled(Container)`
  max-width: calc(1280px + 16px);
  background: #fff;
  box-shadow: 0 0 8px 0 rgba(34, 60, 80, 0.4);
  padding: 8px 0;
  margin: -16px auto 16px;
  position: relative;
  z-index: 10;
  border-radius: 4px;
  @media (max-width: 1380px) {
    max-width: none;
    border-radius: unset;
    margin-bottom: 0;
  }
`
