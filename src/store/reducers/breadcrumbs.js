import * as at from 'store/actionTypes';

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case at.SET_BREADCRUMBS: {
      return { ...action.payload }
    } default: {
      return state
    }
  }
}
