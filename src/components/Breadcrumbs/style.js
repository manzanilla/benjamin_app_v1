import styled from 'styled-components'
import { Container as ContainerHelper } from 'helpers/style'
import { RightArrow } from '@styled-icons/boxicons-solid/RightArrow'

export const Container = styled(ContainerHelper)`
  overflow: hidden;
`

export const Separator = styled(RightArrow)`
  color: #87ceeb;
`

export const Wrap = styled.div`
  margin-top: 12px;
  white-space: nowrap;
  display: flex;
  align-items: center;
  height: 24px;
  a {
    color: #000;
    display: flex;
    align-items: center;
    font-size: 11px;
    font-weight: bold;
    text-transform: uppercase;
    
    /* @todo почему текст не по центру? */
    position: relative;
    top: 1px;
  }
  > * {
    margin-right: 4px;
    :last-child {
      margin-right: 0;
    }
  }    
  > *:first-child {    
    width: 24px;
    height: 24px;
    background: #87ceeb;
    border-radius: 4px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
  }
  :last-child {
    margin-right: 0;
  }
`
