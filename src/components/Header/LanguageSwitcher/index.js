import { connect } from 'react-redux'
import LanguageSwitcher from './LanguageSwitcher'

const mapStateToProps = (state) => ({
  languageId: state.app.languageId,
  locationPath: state.app.locationPath,
  locationPathWithoutLanguage: state.app.locationPathWithoutLanguage
})

export default connect(mapStateToProps)(LanguageSwitcher)
