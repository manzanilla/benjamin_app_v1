import React, { useState, useEffect } from 'react'
import { Link } from 'wouter'
import { Wrap } from './style'
import { LANGUAGE_LIST } from 'helpers/language'

export default (props) => {
  if (LANGUAGE_LIST.length < 2) return null

  const [ locationPath, setLocationPath ] = useState(props.locationPath)

  useEffect(() => {
    if (props.locationPath !== locationPath) {
      setLocationPath(props.locationPath)
    }
  }, [ props.locationPath ])

  return (
    <Wrap className="language-switcher">
      {LANGUAGE_LIST.map(({ CODE, LABEL, IS_DEFAULT, ID }) => {
        const href = IS_DEFAULT
          ? props.locationPathWithoutLanguage
          : props.locationPathWithoutLanguage === '/'
            ? `/${CODE}`
            :`/${CODE}${props.locationPathWithoutLanguage}`

        const linkProps = {}
        linkProps.key = CODE
        linkProps.href = href

        if (ID === props.languageId) {
          linkProps.className = 'active'
        }

        return (
          <Link {...linkProps}>
              {LABEL}
          </Link>
        )
      })}
    </Wrap>
  )
}
