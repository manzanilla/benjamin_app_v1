import axios from 'axios'
import * as at from 'store/actionTypes'

export const setInitialState = () => (dispatch) => {
  try {
    dispatch({ type: at.SET_PRODUCTS_INITIAL_STATE })
  } catch (e) {
    console.error(e)
  }
}

export const getProductsSearch = (languageCode, categoryId) => async (dispatch, getStore) => {
  try {
    const { app: { hostApiV1 } = {} } = getStore()
    const url = `${hostApiV1}/${languageCode}/products/search/mk5values_${categoryId}`

    dispatch({ type: at.GET_PRODUCTS_SEARCH_REQUEST })

    await axios.get(url).then(({ data: payload }) => {
      dispatch({
        type: at.GET_PRODUCTS_SEARCH_SUCCESS,
        languageCode,
        payload
      })
    }).catch((e) => {
      console.error(e)

      dispatch({ type: at.GET_CATEGORIES_TREE_FAILURE })
    })
  } catch (e) {
    console.error(e)

    dispatch({ type: at.GET_PRODUCTS_SEARCH_FAILURE })
  }
}
