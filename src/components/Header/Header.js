import React from 'react'
import LanguageSwitcher from './LanguageSwitcher'
import { Container } from 'helpers/style'
import { Wrap } from './style'
import Cart from './Cart'
import Logo from './Logo'
import Nav from 'components/Nav'
import { getLinkHrefByLanguageCode } from 'helpers/language'
import { Link } from '../Nav/style'
import { TelephoneFill as Telephone } from '@styled-icons/bootstrap/TelephoneFill'

export default (props) => {
  let menuJSX

  if (props.menuIsVisible) {
    const {
      categoriesTree: {
        [props.categoriesLanguageCode]: categoriesTree = []
      } = {}
    } = props

    menuJSX = (
      <Container className="last-container active">
        {categoriesTree.map((el) => {
          const href = `c${el.id}`

          return (
            <Link
              key={href}
              to={getLinkHrefByLanguageCode(props.categoriesLanguageCode, href)}
            >
              {el.name}
            </Link>
          )
        })}
      </Container>
    )
  }

  return (
    <Wrap>
      <Container className="first-container">
        <div>
          <Logo />
          <Nav
            isMain={true}
            context="header-hamburger"
          />
        </div>
        <Nav context="header-horizontal" />
        <div>
          <a className="tel" title="+380978127741" href="tel:+380978127741">
            <Telephone size={24} fill="#87ceeb" />
            <span>+38 (097) 812 77 41</span>
          </a>
          <Cart />
          <LanguageSwitcher />
        </div>
      </Container>
      {menuJSX}
    </Wrap>
  )
}
