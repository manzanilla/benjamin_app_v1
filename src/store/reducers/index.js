import { combineReducers } from 'redux'

import app from 'store/reducers/app'
import breadcrumbs from 'store/reducers/breadcrumbs'
import categories from 'store/reducers/categories'
import products from 'store/reducers/products'

export default combineReducers({
  app,
  breadcrumbs,
  categories,
  products
});
