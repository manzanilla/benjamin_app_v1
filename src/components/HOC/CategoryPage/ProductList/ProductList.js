import React from 'react'
import { Wrap } from './style'
import ProductItem from './ProductItem'

export default (props) => {
  const [ categoryId, setCategoryId ] = React.useState(props.categoryId)
  const [ languageCode, setLanguageCode ] = React.useState(props.languageCode)

  React.useEffect(() => {
    if (props.categoryId !== categoryId) {
      props.search(languageCode, props.categoryId).then(() => {
        setCategoryId(props.categoryId)
      })
    }
    if (props.languageCode !== languageCode) {
      props.search(props.languageCode, categoryId).then(() => {
        setLanguageCode(props.languageCode)
      })
    }
  }, [ props.categoryId, props.languageCode ])

  React.useEffect(() => {
    const { [languageCode]: list } = props.list

    if (!list) {
      props.search(languageCode, categoryId)
    }

    return () => {
      props.setInitialState()
    }
  }, [])

  const { [languageCode]: list = [] } = props.list

  return list.length ? (
    <Wrap>
      {list.map((el) => {
        const {
          id,
          price,
          [`h1_${languageCode}`]: h1,
          mainImage: {
            dir: mainImageDir,
            ext: mainImageExtId,
          } = {}
        } = el

        const mainImage = getMainImage(props.hostStatic, mainImageDir, mainImageExtId)

        return (
          <ProductItem
            key={id}
            id={id}
            h1={h1}
            languageCode={languageCode}
            price={price}
            imageURL={mainImage}
          />
        )
      })}
    </Wrap>
  ) : null
}

function getMainImage(host, mainImageDir, mainImageExtId) {
  if (mainImageDir) {
    const ext = getExtNameByExtId(mainImageExtId)

    if (ext) {
      const fileName = `314x471.${ext}`
      const dir = `${mainImageDir.slice(-1)}/${mainImageDir.slice(-2)}/${mainImageDir}`
      return `${host}/${dir}/${fileName}`
    }
  }
}

function getExtNameByExtId(extId) {
  if (extId === 1) return 'jpg'
  if (extId === 2) return 'png'
  if (extId === 3) return 'webp'
}
