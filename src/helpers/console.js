import chalk from 'chalk'

export const log = function () {
  const messages = []

  for (const argument of Object.values(arguments)) {
    messages.push(chalk.bold.hex('#000').bgKeyword('green')(argument))
  }

  console.log(...messages)
}

export const logError = function () {
  const messages = []

  for (const argument of Object.values(arguments)) {
    messages.push(chalk.bold.hex('#000').bgKeyword('red')(argument))
  }

  console.log(...messages)
}
