import React from 'react'
import { Helmet } from 'react-helmet'
import assets from '../../../webpack-assets.json'

export default (props) => {
  const helmet = Helmet.renderStatic()
  const htmlAttributes = helmet.htmlAttributes.toComponent()
  const bodyAttributes = helmet.bodyAttributes.toComponent()
  const rootProps = { id: 'root' }

  if (props.__html) {
    rootProps.dangerouslySetInnerHTML = { __html: props.__html }
  }

  const {
    app: {
      hostStatic,
      hostStaticLocal,
      mode
    } = {},
  } = props.state;

  let scriptFileHost
  if (mode === 'development') {
    scriptFileHost = hostStaticLocal
  } else {
    scriptFileHost = hostStatic
  }

  const { js: appJsSrc } = assets.app
  const { js: vendorJsSrc } = assets.vendor

  return (
    <html {...htmlAttributes}>
    <head>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0"
      />
      <link rel="shortcut icon" href={`${hostStatic}/favicon.ico`} />
      <link rel="apple-touch-icon" sizes="180x180" href={`${hostStatic}/apple-touch-icon.png`} />
      <link rel="icon" type="image/png" sizes="32x32" href={`${hostStatic}/favicon-32x32.png`} />
      <link rel="icon" type="image/png" sizes="16x16" href={`${hostStatic}/favicon-16x16.png`} />
      <link rel="manifest" href={`${hostStatic}/manifest.json`} />
      { helmet.title.toComponent() }
      { helmet.meta.toComponent() }
      { helmet.link.toComponent() }
      { props.styleElement ? props.styleElement : null }
    </head>
    <body {...bodyAttributes}>
      <div {...rootProps} />
      <script
        dangerouslySetInnerHTML={{
          __html: `window.__PRELOADED_STATE__=${ JSON.stringify(props.state) }`
        }}
      />
      <script src={`${scriptFileHost}${vendorJsSrc}`} />
      <script src={`${scriptFileHost}${appJsSrc}`} />
    </body>
    </html>
  )
}
