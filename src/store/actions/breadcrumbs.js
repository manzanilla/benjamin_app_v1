import * as at from 'store/actionTypes'

export const setBreadcrumbs = (payload) => (dispatch) => {
  dispatch({ type: at.SET_BREADCRUMBS, payload })
}
