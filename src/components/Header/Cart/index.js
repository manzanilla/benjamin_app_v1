import { connect } from 'react-redux'
import Cart from './Cart'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode
})

export default connect(mapStateToProps)(Cart)
