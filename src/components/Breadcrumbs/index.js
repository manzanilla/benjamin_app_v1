import { connect } from 'react-redux'
import Breadcrumbs from './Breadcrumbs'

const mapStateToProps = (state) => {
  return {
    breadcrumbs: state.breadcrumbs
  }
}

export default connect(mapStateToProps)(Breadcrumbs)
