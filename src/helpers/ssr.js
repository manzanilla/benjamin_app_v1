import React from 'react'
import { renderToString, renderToStaticMarkup } from 'react-dom/server'
import Html from 'components/Html'
import { Router } from 'wouter'
import staticLocationHook from 'wouter/static-location'
import { Provider } from 'react-redux'
import { ServerStyleSheet, StyleSheetManager } from 'styled-components'
import App from 'components/App'

export default function (ctx, store) {
  const sheet = new ServerStyleSheet()
  const props = {}

  props.state = store.getState()
  props.__html = renderToString(
    <Router hook={staticLocationHook(ctx.path)}>
      <Provider store={store}>
        <StyleSheetManager sheet={sheet.instance}>
          <App />
        </StyleSheetManager>
      </Provider>
    </Router>
  )
  props.styleElement = sheet.getStyleElement()

  return '<!doctype html>'+renderToStaticMarkup(<Html {...props} />)
}
