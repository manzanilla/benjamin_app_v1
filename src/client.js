import React from 'react'
import { hydrate } from 'react-dom'
import App from 'components/App'
import { Provider } from 'react-redux'
import createStore from 'store/createStore'

const initialState = window.__PRELOADED_STATE__
delete window.__PRELOADED_STATE__
const store = createStore(initialState)
const container = document.getElementById('root')

const element = (
  <Provider store={store}>
    <App />
  </Provider>
)

hydrate(element, container)
