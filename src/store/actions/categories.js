import axios from 'axios'
import * as at from 'store/actionTypes'
import querystring from 'querystring'

export const getCategoriesTree = (languageCode) => async (dispatch, getStore) => {
  try {
    const { app: { hostApiV1 } = {} } = getStore()
    const qs = querystring.stringify({
      languageCode,
      status: 1,
      format: 'tree'
    })
    const url = `${hostApiV1}/categories?${qs}`

    dispatch({ type: at.GET_CATEGORIES_TREE_REQUEST })

    const { data: tree } = await axios.get(url)

    dispatch({
      type: at.GET_CATEGORIES_TREE_SUCCESS,
      languageCode,
      tree
    })
  } catch (e) {
    console.error(e)

    dispatch({ type: at.GET_CATEGORIES_TREE_FAILURE })
  }
}

export const setLanguageCode = (languageCode) => (dispatch, getStore) => {
  try {
    const { categories } = getStore()

    if (categories.languageCode !== languageCode) {
      dispatch({ type: at.CATEGORIES_SET_LANGUAGE_CODE, languageCode })
    }
  } catch (e) {
    console.error(e)
  }
}

export const setTreeMap = (languageCode) => (dispatch, getStore) => {
  try {
    const { categories } = getStore()
    const { tree: { [languageCode]: tree = [] } } = categories
    const treeMap = {}

    const setMap = (tree, parents = []) => {
      tree.forEach((el, i) => {
        const { id, children } = el
        treeMap[id] = [...parents]
        treeMap[id].push(i)

        if (children) {
          setMap(children, treeMap[id])
        }
      })
    }

    setMap(tree)

    dispatch({ type: at.SET_CATEGORIES_TREE_MAP, treeMap })
  } catch (e) {
    console.error(e)
  }
}
