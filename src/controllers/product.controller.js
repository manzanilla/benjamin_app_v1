import ssr from 'helpers/ssr'

export default async (props) => {
  const { ctx, store } = props

  // let { id: productId } = ctx.params

  ctx.body = ssr(ctx, store)
}
