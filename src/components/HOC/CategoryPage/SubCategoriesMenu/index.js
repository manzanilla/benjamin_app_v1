import { connect } from 'react-redux'
import SubCategoriesMenu from './SubCategoriesMenu'

const mapStateToProps = (state) => {
  return {
    tree: state.categories.tree,
    treeMap: state.categories.treeMap,
    categoriesLanguageCode: state.categories.languageCode
  }
}

export default connect(mapStateToProps)(SubCategoriesMenu)
