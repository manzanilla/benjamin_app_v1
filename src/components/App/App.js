import React, { useEffect } from 'react'
import { Switch, Route, useLocation } from 'wouter'

import IndexPage from 'components/HOC/IndexPage'
import CategoryPage from 'components/HOC/CategoryPage'
import CartPage from 'components/HOC/CartPage'
import ProductPage from 'components/HOC/ProductPage'
import GlobalStyles from 'helpers/globalStyles'
import Header from 'components/Header'
import {
  getLanguageInfoFromLocationPath,
  getLocationPathWithoutLanguage
} from 'helpers/language'
import { LANGUAGE_UK } from 'helpers/language'
import { Wrap } from './style'
import Footer from 'components/Footer'

export default (props) => {
  const [ location ] = useLocation()

  useEffect(() => {
    if (location !== props.locationPath) {
      const {
        id: languageId,
        code: languageCode
      } = getLanguageInfoFromLocationPath(location)

      const params = {
        locationPath: location,
        locationPathWithoutLanguage: getLocationPathWithoutLanguage(languageId, location),
        languageId,
        languageCode
      }

      props.onLocationPathChange(params)
    }
  }, [ location ])

  return (
    <>
      <Wrap>
        <Header />
        <GlobalStyles />
        <Switch>
          <Route path={`/`}>
            <IndexPage />
          </Route>
          <Route path={`/${LANGUAGE_UK.CODE}`}>
            <IndexPage />
          </Route>
          <Route path={`/cart`}>
            <CartPage />
          </Route>
          <Route path={`/${LANGUAGE_UK.CODE}/cart`}>
            <CartPage />
          </Route>
          <Route path={`/p:id`}>
            {(props) => (
              <ProductPage id={props.id} />
            )}
          </Route>
          <Route path={`/${LANGUAGE_UK.CODE}/p:id`}>
            {(props) => (
              <ProductPage id={props.id} />
            )}
          </Route>
          <Route path={`/c:id/:rest*`}>
            {(props) => (
              <CategoryPage categoryId={+props.id} />
            )}
          </Route>
          <Route path={`/${LANGUAGE_UK.CODE}/c:id/:rest*`}>
            {(props) => (
              <CategoryPage categoryId={+props.id} />
            )}
          </Route>
        </Switch>
      </Wrap>
      <Footer />
    </>
  )
}
