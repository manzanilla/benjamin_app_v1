import { connect } from 'react-redux'
import IndexPage from './IndexPage'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode
})

export default connect(mapStateToProps)(IndexPage)
