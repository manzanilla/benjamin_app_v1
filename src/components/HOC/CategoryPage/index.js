import { connect } from 'react-redux'
import CategoryPage from './CategoryPage'
import * as breadcrumbsActionCreators from 'store/actions/breadcrumbs'

const mapStateToProps = (state) => ({
  languageCode: state.app.languageCode,
  categories: state.categories
})

const mapDispatchToProps = (dispatch) => ({
  setBreadcrumbs: (payload) => dispatch(breadcrumbsActionCreators.setBreadcrumbs(payload))
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoryPage)
