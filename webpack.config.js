const { resolve } = require('path')
const nodeExternals = require('webpack-node-externals')
const AssetsPlugin = require('assets-webpack-plugin')
const CompressionPlugin = require('compression-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

require('dotenv').config({
  path: resolve(__dirname, '.env')
})

const isDev = process.env.NODE_ENV === 'development'

const js = {
  test: /\.js$/,
  exclude: /node_modules/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: [
        '@babel/preset-env',
        '@babel/preset-react'
      ]
    }
  }
}

const ts = {
  test: /\.tsx?$/,
  use: 'ts-loader',
  exclude: /node_modules/
}

const wpResolve = {
  modules: [
    resolve(__dirname, 'src'),
    resolve(__dirname, 'node_modules')
  ],
  extensions: ['.tsx', '.ts', '.js']
}

const wpPlugins = [
  new CleanWebpackPlugin(),
  new AssetsPlugin()
]

if (!isDev) {
  wpPlugins.push(
    new CompressionPlugin({
      algorithm: 'gzip',
      compressionOptions: { level: 6 }
    })
  )
}

const serverConfig = {
  mode: process.env.NODE_ENV,
  target: 'node',
  resolve: wpResolve,
  node: {
    __dirname: false
  },
  externals: [nodeExternals()],
  entry: {
    server: resolve(__dirname, 'src', 'server.js')
  },
  module: {
    rules: [js, ts]
  },
  output: {
    path: resolve(__dirname, 'dist'),
    filename: '[name].bundle.js'
  }
}

const clientConfig = {
  mode: process.env.NODE_ENV,
  target: 'web',
  resolve: wpResolve,
  devtool: isDev ? 'cheap-module-source-map' : 'source-map',
  entry: {
    app: resolve(__dirname, 'src/client.js')
  },
  module: {
    rules: [js, ts]
  },
  output: {
    path: resolve(__dirname, 'dist', 'public'),
    filename: '[name].[fullhash].js',
    publicPath: '/'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          chunks: 'initial',
          minChunks: 2,
          maxInitialRequests: 5, // The default limit is too small to showcase the effect
          minSize: 0 // This is example is too small to create commons chunks
        },
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          priority: 10,
          enforce: true
        }
      }
    }
  },
  plugins: wpPlugins
}

module.exports = [ clientConfig, serverConfig ]
